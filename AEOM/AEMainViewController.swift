//
//  ASMainViewController.swift
//  AEOM
//
//  Created by Sanchan on 20/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//
import UIKit


protocol CarousalSelectDelegate: class
{
    func carousalSelected(carousalName:String,carousalDetailDict:[[String:Any]],userid:String,deviceid:String)
    func collectionItemSelected(forUrl:String)
}

class AEMainViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,myListdelegate{
    
    var CarousalName = NSMutableArray()
    var CategoryCarousalName = NSMutableArray()
    var searchCollectionList = NSMutableArray()
    var deviceId,userId,uuid : String!
    var myList = [[String:Any]]()
    var CarousalData = [[String:Any]]()
    var MenuCarousalData = [[String:Any]]()
    var recentList = [[String:Any]]()
    weak var delegate:CarousalSelectDelegate?
    var storeProdData = [[String:Any]]()
    var PhotoDict = [[String:Any]]()
    
    @IBOutlet weak var MainImage1: UIImageView!
    @IBOutlet var listView:UITableView!
    @IBOutlet weak var MainImage: UIImageView!
    @IBOutlet weak var AssestName: UILabel!
    @IBOutlet weak var ReleaseDate: UILabel!
    @IBOutlet weak var TimeLbl : UILabel!
    @IBOutlet weak var Description: UITextView!
    @IBOutlet var staticLbl: UILabel!
    @IBOutlet var lbl2:UILabel!
    var activityView = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
        activityView = ASActivityView.init(frame: self.view.frame)
        self.view.addSubview(activityView)
        
        (self.view.viewWithTag(1))?.isHidden = true
        getFetchMyList(withurl: kFetchMyListUrl)
        getFetchRecentList(withurl: kFetchRecentUrl)
        StoreDetails()
        getPhotos()
        getcarosuel(withurl: kCarouselUrl)
        self.CarousalName.add("Menu")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.CarousalName.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = AECollectionContentCell()
        // For Menu Collection view
        MenuCarousalData = [["metadata": ["carousel_id":"Menu","movie_art":UIImage.init(named: "categories_small")!,kIsmenu: true,"main_carousel_image_url":"https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/ebef793c-53f9-40a7-83f2-813fb2fb2b2f.png"]],["metadata": ["carousel_id":"Menu","movie_art":UIImage.init(named: "settings_small")!,kIsmenu: true,"main_carousel_image_url":"https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/ebef793c-53f9-40a7-83f2-813fb2fb2b2f.png"]],
                            ["metadata": ["carousel_id":"Menu","movie_art":UIImage.init(named: "searchicon")!,kIsmenu: true,"main_carousel_image_url":"https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/ebef793c-53f9-40a7-83f2-813fb2fb2b2f.png"]]]
        
        if indexPath.section == 0
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! AECollectionContentCell
            cell.carousalSelected(carousalName: "Menu", carousalDetailDict: MenuCarousalData,userid:userId,deviceid: deviceId,uuid:uuid)
            
        }
        else
        {
            if indexPath.section == 1
            {
                if !(myList.isEmpty)
                {
                    cell = tableView.dequeueReusableCell(withIdentifier: "myListCell", for: indexPath) as! AECollectionContentCell
                    cell.carousalSelected(carousalName: CarousalName[indexPath.section] as! String, carousalDetailDict: myList, userid: userId, deviceid: deviceId,uuid: uuid)
                }
                else
                {
                    if !(recentList.isEmpty)
                    {
                        cell = tableView.dequeueReusableCell(withIdentifier: "recentCell", for: indexPath) as! AECollectionContentCell
                        cell.carousalSelected(carousalName: CarousalName[indexPath.section] as! String, carousalDetailDict: recentList, userid: userId, deviceid: deviceId,uuid: uuid)
                    }
                    else
                    {
                        cell = tableView.dequeueReusableCell(withIdentifier: "carousalCell", for: indexPath) as! AECollectionContentCell
                        cell.carousalSelected(carousalName: CarousalName[indexPath.section] as! String, carousalDetailDict: CarousalData,userid: userId,deviceid: deviceId,uuid: uuid)
                    }
                }
            }
            else if indexPath.section == 2
            {
                if !(recentList.isEmpty)
                {
                    if CarousalName.contains("My List")
                    {
                        cell = tableView.dequeueReusableCell(withIdentifier: "recentCell", for: indexPath) as! AECollectionContentCell
                        cell.carousalSelected(carousalName: CarousalName[indexPath.section] as! String, carousalDetailDict: recentList, userid: userId, deviceid: deviceId,uuid:uuid)
                    }
                    else
                    {
                        cell = tableView.dequeueReusableCell(withIdentifier: "carousalCell", for: indexPath) as! AECollectionContentCell
                        cell.carousalSelected(carousalName: CarousalName[indexPath.section] as! String, carousalDetailDict: CarousalData, userid: userId, deviceid: deviceId,uuid:uuid)
                    }
                }
                else
                {
                    cell = tableView.dequeueReusableCell(withIdentifier: "carousalCell", for: indexPath) as! AECollectionContentCell
                    cell.carousalSelected(carousalName: CarousalName[indexPath.section] as! String, carousalDetailDict: CarousalData,userid: userId,deviceid: deviceId,uuid:uuid)
                }
            }
                
            else
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "carousalCell", for: indexPath) as! AECollectionContentCell
                
                if indexPath.section == (CarousalName.count - 2)
                {
                    cell.carousalSelected(carousalName: CarousalName[indexPath.section] as! String, carousalDetailDict: PhotoDict , userid: userId, deviceid: deviceId, uuid: uuid)
                }
                else if indexPath.section == (CarousalName.count - 1)
                {
                    cell.carousalSelected(carousalName: CarousalName[indexPath.section] as! String, carousalDetailDict: storeProdData , userid: userId, deviceid: deviceId, uuid: uuid)
                }
                else
                {
                    cell.carousalSelected(carousalName: CarousalName[indexPath.section] as! String, carousalDetailDict: CarousalData,userid: userId,deviceid: deviceId,uuid:uuid)
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 27
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view1 = UIView(frame: CGRect(x: 45, y: 0, width: tableView.frame.width, height: 27))
        let header = UILabel(frame: CGRect(x: 45, y:0 , width: tableView.frame.width, height: 27))
        header.tag = 100
        header.font = UIFont.init(name: "Helvetica-Bold", size: 25)
        header.textAlignment = .left
        header.text = CarousalName[section] as? String
        header.textColor = UIColor.white
        view1.addSubview(header)
        return view1
    }
    func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    
    func getcarosuel(withurl:String)
    {
        var parameters = [String:[String:AnyObject]]()
        parameters = ["getCarousels":["id":deviceId as AnyObject , "userType":kUserType as AnyObject]]
        AEApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters){(responseDict, error,isDone) in
            if error == nil
            {
                let post = responseDict as! [[String:Any]]
                for dict in post
                {
                    let carousalObj = Carousal().initWithName(name: dict["carousel_name"] as! String, mobile: dict["mobile"] as! String, order: String(describing: dict["order"]), site: dict["site"] as! String, status: dict["status"] as! String, tv: dict["tv"] as! String)
                    if (carousalObj.carousel_name == "Photos") || (carousalObj.carousel_name == "Store")
                    {
                        print("\(carousalObj.carousel_name) is Skipped")
                    }
                    else
                    {
                        self.CarousalName.add(carousalObj.carousel_name)
                        self.CategoryCarousalName.add(carousalObj.carousel_name)
                    }
                }
                self.CarousalName.add("Photos")
                self.CarousalName.add("Store")
                self.CategoryCarousalName.add("Photos")
                self.CategoryCarousalName.add("Store")
                for i in 0..<self.CarousalName.count
                {
                    if ((self.CarousalName[i] as! String) == "Menu") || ((self.CarousalName[i] as! String) == "My List") || ((self.CarousalName[i] as! String) == "Recently Watched") || ((self.CarousalName[i] as! String) == "Store") || ((self.CarousalName[i] as! String) == "Photos")
                    {
                        print(self.CarousalName[i])
                    }
                    else
                    {
                        self.getCarousalDetails(name: kCarouselDataUrl,carousal:self.CarousalName[i] as! String)
                    }
            }
                
            }
            else
            {
                let alertview = UIAlertController(title: "Server Down", message: "Due to some Techincal Problem Server Down" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                    // exit(0)
                })
                alertview.addAction(defaultAction)
                self.present(alertview, animated: true, completion:nil)
            }
        }
    }
    
    func getCarousalDetails(name:String,carousal:String)
    {
        var parameters = [String:[String:AnyObject]]()
        parameters =  ["getCarouselData":["name":(carousal as AnyObject),"id":deviceId as AnyObject, "userType":kUserType as AnyObject]]
        AEApiManager.sharedManager.postDataWithJson(url: name, parameters: parameters){(responseDict, error, isDone) in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                let arr = JSON[carousal]
                
                for dict in arr as! [[String:Any]]
                {
                    let dicton = dict as NSDictionary
                    self.CarousalData.append(dicton as! [String : Any])
                    self.searchCollectionList.add(dicton)
                }
                DispatchQueue.main.async
                    {
                        self.listView.reloadData()
                }
               
            }
            else
            {
               // print("json Error")
//                if carousal == "Menu"
//                {
//                    
//                }
//                else if carousal == "My List"
//                {
//                    
//                }
//                else if carousal == "Recently Watched"
//                {
//                    
//                }
//                
//                else
//                {
                    let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                        UIAlertAction in
                    let _ = self.navigationController?.popToRootViewController(animated: true)
                    })
                    alertview.addAction(defaultAction)
                    self.present(alertview, animated: true, completion: nil)
               // }
            }
        }
        activityView.removeFromSuperview()
    }
    
    func getFetchMyList(withurl:String)
    {
        var parameters = [String:[String:AnyObject]]()
        parameters = ["fetchMyList":[ "userId": userId as AnyObject , "id":deviceId as AnyObject, "userType":kUserType as AnyObject]]
        AEApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters){(responseDict, error, isDone) in
            if error ==  nil
            {
                let JSON = responseDict as! NSDictionary
                let arr = JSON["myList"] as! NSArray
                for dict in arr as! [[String:Any]]
                {
                    let dicton = dict as NSDictionary
                    self.myList.append(dicton as! [String:Any])
                }
                DispatchQueue.main.async
                    {
                        if self.CarousalName.contains("My List")
                        {
                            self.listView.reloadData()
                        }
                        else
                        {
                            if  !(self.myList.isEmpty)
                            {
                                self.CarousalName.insert("My List", at: 1)
                            }
                        }
                        self.listView.reloadData()
                }
            }
            else
            {
                print("JSON ERROR")
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                })
                alertview.addAction(defaultAction)
                self.present(alertview, animated: true, completion: nil)
            }
        }
    }

    // Service Call for getFetchRecentList
    func getFetchRecentList(withurl: String)
    {
        var parameters = [String:[String:AnyObject]]()
        parameters = ["fetchRecentlyWatched":["userId": userId as AnyObject, "userType": kUserType as AnyObject,"id":deviceId as AnyObject]]
        AEApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters){(responseDict, error, isDone) in
            if error ==  nil
            {
                let JSON = responseDict as! NSDictionary
                let arr = JSON["recentlyWatched"] as! NSArray
                self.recentList.removeAll()
                for dict in arr as! [[String:Any]]
                {
                    let dicton = dict as NSDictionary
                    self.recentList.append(dicton as! [String:Any])
                }
                DispatchQueue.main.async
                    {
                        if self.CarousalName.contains("Recently Watched")
                        {
                            self.listView.reloadData()
                        }
                        else
                        {
                            if  !(self.recentList.isEmpty)
                            {
                                if self.CarousalName.contains("My List")
                                {
                                    self.CarousalName.insert("Recently Watched", at: 2)
                                }
                                else
                                {
                                    self.CarousalName.insert("Recently Watched", at: 1)
                                }
                            }
                        }
                        self.listView.reloadData()
                }
            }
            else
            {
                print("JSON ERROR")
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    func StoreDetails()
    {
        AEApiManager.sharedManager.getDataWithJson(url:kStoreUrl)
        {
            (responseDict) in
            if responseDict != nil
            {
                let post = responseDict as! NSDictionary
                let data = post.value(forKey: "dataa") as! [[String:Any]]
                self.storeProdData = data
            }
            else
            {
                
            }
        }
    }
    
    func getPhotos()
    {
        AEApiManager.sharedManager.getDataWithJson(url:kgetPhoto)
        {
            (responseDict) in
            if responseDict != nil
            {
                let post = responseDict as! NSDictionary
                let dict = post.value(forKey: "photos") as! [[String:Any]]
                self.PhotoDict = dict
            }
            else
            {
                
            }
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func myListdata(mylistDict: NSDictionary)
    {
        myList.append(mylistDict as! [String : Any])
        if myList.isEmpty
        {
            print("mylist is empty")
        }
        else
        {
            if CarousalName.contains("My List")
            {
                listView.reloadData()
            }
            else
            {
                CarousalName.insert("My List", at: 1)
            }
        }
        listView.reloadData()
    }
    
    func removeListdata(id:String) {
        var i:Int = 0
        for dict in myList
        {
            let dicton = dict["data"] as! NSDictionary
            let assetid = dicton["id"] as! String
            if assetid == id
            {
                myList.remove(at: i)
                if myList.isEmpty
                {
                    CarousalName.remove("My List")
                }
            }
            i=i+1
        }
        listView.reloadData()
    }
    
    func recentlyWatcheddata(recentlyWatchedDict:NSDictionary)
    {
        getFetchRecentList(withurl: kFetchRecentUrl)
        if !(recentList.isEmpty)
        {
            if CarousalName.contains("Recently Watched")
            {
                listView.reloadData()
            }
            else
            {
                if CarousalName.contains("My List")
                {
                    CarousalName.insert("Recently Watched", at: 2)
                }
                else
                {
                    CarousalName.insert("Recently Watched", at: 1)
                }
            }
        }
        listView.reloadData()
    }
}

//Extension Methods for myListDelegate
//extension AEMainViewController:myListdelegate
//{
//   
//}



