//
//  ASAccountInfoViewController.swift
//  AEOM
//
//  Created by Sanchan on 20/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit

class AEAccountInfoViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    
    var deviceId,uuid: String!
    var accountDict = NSDictionary()
    var accountMenu = [String]()
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var logoImage: UIImageView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        accountMenu.append("Email")
        accountMenu.append("Subscription Type")
        accountMenu.append("Subscription End")
        tableView.isUserInteractionEnabled = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accountMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        (cell.viewWithTag(10) as! UILabel).text = accountMenu[indexPath.row]
        switch  indexPath.row
        {
        case 0:
            (cell.viewWithTag(11) as! UILabel).text = (accountDict.object(forKey: "email") as! String)
            break
        case 1:
            (cell.viewWithTag(11) as! UILabel).text = (accountDict.object(forKey: "subscription_type") as! String)
            break
        case 2:
            let dateformatter = DateFormatter()
            dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let date = dateformatter.date(from: (accountDict.object(forKey: "subscription_end") as! String))
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/YYYY"
            (cell.viewWithTag(11) as! UILabel).text = formatter.string(from: date!)
            break
        default:
            break
        }
        cell.layer.cornerRadius = 7.0
        cell.textLabel?.textColor = UIColor.black
        return cell
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
