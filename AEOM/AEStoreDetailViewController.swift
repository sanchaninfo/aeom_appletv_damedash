//
//  DetailPageViewController.swift
//  PumaStore
//
//  Created by Sanchan on 20/01/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit
import Kingfisher

class AEStoreDetailViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var largeImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var desTitle: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var selectedsize: UILabel!
    @IBOutlet weak var clr: UILabel!
    @IBOutlet weak var quanView: UIView!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var colorCollectionView: UICollectionView!
    
    
    var userId = String()
    var uuid,deviceId: String!
    var firstView:UIView!
    var sizeView:UIView!
    var productlist = NSDictionary()
    var colorBtn: UIButton!
    var sizeBtn: UIButton!
    var produtID = String()
    var btn: UIButton!
    var xvalue: Int = 0
    var xcolor:Int = 0
    var sizes = NSArray()
    var colorCode = String()
    var viewToFocus: UIView? = nil {
        didSet {
            if viewToFocus != nil {
                self.setNeedsFocusUpdate();
                self.updateFocusIfNeeded();
            }
        }
    }
   override var preferredFocusEnvironments : [UIFocusEnvironment] {
        //Condition
        return [colorCollectionView]
    }
    override func viewDidLoad() {
    
        super.viewDidLoad()
        
        let str = kStoreBaseUrl +  (((productlist["images"] as! NSArray)[0] as! NSDictionary)["large"] as! String)
        largeImage.kf.setImage(with: URL(string:str))
        let attr = NSAttributedString(string: "$\(productlist["price"] as! String)" , attributes: [NSForegroundColorAttributeName:(UIColor.init(red: 200/255, green: 169/255, blue: 133/255, alpha: 1.0))])
                continueBtn.setAttributedTitle(attr, for: UIControlState.normal)
        productTitle.text = (productlist["title"] as! String)
        desTitle.text = (productlist["description"] as! String)
        continueBtn.layer.cornerRadius = 5.0
        minusBtn.layer.cornerRadius = min(minusBtn.frame.size.height, minusBtn.frame.size.width)/2.0
        minusBtn.clipsToBounds = true
        plusBtn.layer.cornerRadius = min(plusBtn.frame.size.height, plusBtn.frame.size.width)/2.0
        plusBtn.clipsToBounds = true
        
        quantityLbl.layer.borderColor = focusColor
        quantityLbl.layer.borderWidth = 3.0
        
        // for Product size view
        sizeView = UIView(frame: CGRect(x: 1223, y: 510, width: 550, height: 80))
        sizeView.backgroundColor = UIColor.white
        self.view.addSubview(sizeView)
        
        
        sizes = productlist["sizes"] as! NSArray
        var xVal2 = CGFloat()
        xVal2 = 0.0
        for i in 0..<(sizes.count)
        {
            let btn = UIButton(type: .custom)
            btn.frame = CGRect(x: xVal2, y: 15, width: 100, height: 50)
            btn.setTitle("\(sizes[i])", for: .normal)
            btn.setTitleColor(UIColor.white, for: .normal)
            btn.titleLabel?.font = UIFont.init(name: "Helvetica", size: 20)
            btn.backgroundColor = UIColor.init(red: 27/255, green: 27/255, blue: 27/255, alpha: 1.0)
            btn.layer.cornerRadius = 5.0
            sizeView.addSubview(btn)
            xVal2 = xVal2+btn.frame.size.width+10
        }
        
 
        let topButtonFocusGuide = UIFocusGuide()
        if #available(tvOS 10.0, *) {
            topButtonFocusGuide.preferredFocusEnvironments = [plusBtn]
        } else {
            // Fallback on earlier versions
        }
        self.view.addLayoutGuide(topButtonFocusGuide)
        self.view.addConstraints([topButtonFocusGuide.topAnchor.constraint(equalTo: quanView.topAnchor), topButtonFocusGuide.bottomAnchor.constraint(equalTo: quanView.bottomAnchor), topButtonFocusGuide.leadingAnchor.constraint(equalTo: quanView.leadingAnchor), topButtonFocusGuide.widthAnchor.constraint(equalTo: quanView.widthAnchor)])
        
    }
    
    // color CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            let colors = productlist["colors"] as! NSArray
            return colors.count
          }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let colors = productlist["colors"] as! NSArray
        let color2 = colorWithHexString(hexString: colors[indexPath.row] as! String)
        (cell.viewWithTag(10) as! UIImageView).backgroundColor = color2
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if let prev = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: prev)
        {
             (cell.viewWithTag(10) as! UIImageView).bounds = CGRect(x: cell.bounds.origin.x, y: cell.bounds.origin.y, width: cell.bounds.size.width, height: cell.bounds.size.width)
               (cell.viewWithTag(10) as! UIImageView).layer.shadowRadius = 0.0
        }
        if let next = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: next)
        {
            
            (cell.viewWithTag(10) as! UIImageView).bounds = CGRect(x: cell.bounds.origin.x, y: cell.bounds.origin.y, width: cell.bounds.size.width + 15, height: cell.bounds.size.width + 15)
            
            cell.layer.shadowColor = UIColor.gray.cgColor
            cell.layer.shadowRadius = 30
            cell.layer.shadowOffset = CGSize(width: 20, height: 20)
            cell.layer.shadowOpacity = 1
            cell.layer.masksToBounds = false
            clr.backgroundColor = (cell.viewWithTag(10) as! UIImageView).backgroundColor
        }
    }
       func colorWithHexString(hexString: String)->UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }

    @IBAction func DECREMENT(_ sender: AnyObject) {
        var decrement = Int(quantityLbl.text!)
        if decrement! > 1
        {
            decrement = decrement! - 1
            quantityLbl.text = String(describing: decrement!)
        }
    }
    
    
    @IBAction func INCREMENT(_ sender: AnyObject) {
        let increment = Int(quantityLbl.text!)! + 1
        quantityLbl.text = String(increment)
    }
 
    
    @IBAction func continueBtn(_ sender: AnyObject) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let productConfirmpage = storyBoard.instantiateViewController(withIdentifier: "ProductConfirm") as! AEStoreProductConfirmViewController
        productConfirmpage.largeImageUrl = kStoreBaseUrl +  (((productlist["images"] as! NSArray)[0] as! NSDictionary)["large"] as! String)
        productConfirmpage.desc = (productlist["description"] as! String)
        productConfirmpage.price = "$" + "   " + "\(productlist["price"] as! String)"
        productConfirmpage.producttl = (productlist["title"] as! String)
        productConfirmpage.quantity = quantityLbl.text
        productConfirmpage.tot = (productlist["price"] as! String)
        productConfirmpage.size = selectedsize.text
        productConfirmpage.color = clr.backgroundColor
        productConfirmpage.userId = userId
        productConfirmpage.productID = productlist["id"] as! String
        productConfirmpage.colorCode = self.colorCode
        productConfirmpage.uuid = uuid
        productConfirmpage.deviceId = deviceId
        self.navigationController?.pushViewController(productConfirmpage, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if (context.nextFocusedView?.isKind(of: UIButton.self))!
        {
            let btn = context.nextFocusedView as! UIButton

            if context.nextFocusedView?.superview == sizeView
            {
                    self.selectedsize.text = btn.currentTitle
                    btn.layer.borderWidth = 5.0
                    btn.layer.borderColor = focusColor
            }
            if context.nextFocusedView == self.continueBtn
            {
                btn.layer.borderWidth = 5.0
                btn.layer.borderColor = focusColor
            }
            if context.nextFocusedView == self.plusBtn
            {
                btn.layer.borderWidth = 5.0
                btn.layer.borderColor = focusColor

            }
            if context.nextFocusedView == self.minusBtn
            {
                btn.layer.borderWidth = 5.0
                btn.layer.borderColor = focusColor
            }
            
        }
        if context.previouslyFocusedView != nil
        {
            if (context.previouslyFocusedView?.isKind(of: UIButton.self))!
            {
                    let btn = context.previouslyFocusedView as! UIButton
                    btn.backgroundColor = context.previouslyFocusedView?.backgroundColor
                    btn.layer.borderWidth = 0.0
            }
        }
    }

 }
